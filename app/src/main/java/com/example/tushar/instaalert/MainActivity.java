package com.example.tushar.instaalert;


import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentManager;

import com.example.tushar.instaalert.dummy.DummyContent;
import com.example.tushar.instaalert.fragment.NotificationFragment;
import com.example.tushar.instaalert.fragment.ReviewFragment;

public class MainActivity extends AppCompatActivity implements NotificationFragment.OnListFragmentInteractionListener,ReviewFragment.OnFragmentInteractionListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadFragmant("Review",null);
    }

    protected void loadFragmant(String tag,String extra) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        switch (tag) {
            case "Review":
                ReviewFragment llf = ReviewFragment.newInstance(extra,"");
                ft.replace(R.id.flFragmentLoader, llf);
                break;

            case "Notify":
            default:
                NotificationFragment nnf = new NotificationFragment();
                ft.replace(R.id.flFragmentLoader, nnf);
                break;
        }
        ft.commit();
    }

    @Override
    public void onListFragmentInteraction(DummyContent.DummyItem item) {
        loadFragmant("Review",item.id);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
